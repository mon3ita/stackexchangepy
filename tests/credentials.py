import re

# To try all tests, you must provide access_token and key.
# To do that, make a file in the main folder,
# and pass it in the method credentials called in test_modules & test_networks.
# The file must have the following format:
# token=your_token
# key=your_key
def credentials(tokens='tokens.txt'):
  token, key = None, None
  with open(tokens, 'r') as f:
    lines = f.readlines()
    token = lines[0].split('=', )[1].strip()
    key = lines[1].split('=')[1].strip()

  return (token, key)
