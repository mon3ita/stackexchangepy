import unittest

from stackexchangepy.client import StackExchangeClient
from tests.credentials import credentials

class TestNetwork(unittest.TestCase):

  def setUp(self):
    token, key = credentials()
    self.client = StackExchangeClient(access_token=token, key=key)

  def test_errors_all(self):
    result, _ = self.client.network.errors.all()
    self.assertTrue(len(result) > 0)

  def test_sites(self):
    result, _ = self.client.network.sites.all()
    self.assertTrue(len(result) > 0)

  def test_notifications(self):
    result, _ = self.client.network.notifications.all()
    self.assertTrue(len(result) > 0)
