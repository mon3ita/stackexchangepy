import unittest

from stackexchangepy.client import StackExchangeClient
from tests.credentials import credentials

class TestTags(unittest.TestCase):

  def setUp(self):
    token, key = credentials()
    self.client = StackExchangeClient(access_token=token, key=key)
