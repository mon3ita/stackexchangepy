import unittest

from stackexchangepy.client import StackExchangeClient
from tests.credentials import credentials

class TestComments(unittest.TestCase):

  def setUp(self):
    token, key = credentials()
    self.client = StackExchangeClient(access_token=token, key=key)

  def test_comments_all(self):
    comments, _ = self.client.comments.all()
    self.assertTrue(len(comments) > 0)