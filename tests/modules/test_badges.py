import unittest

from stackexchangepy.client import StackExchangeClient
from tests.credentials import credentials

class TestBadges(unittest.TestCase):

  def setUp(self):
    token, key = credentials()
    self.client = StackExchangeClient(access_token=token, key=key)

  def test_badges_all(self):
    badges = self.client.badges.all()
    self.assertEqual(len(badges), 2)

  def test_badges_ids(self):
    badges = self.client.badges.ids([123, 234])
    self.assertEqual(len(badges), 2)

  def test_badges_name(self):
    badges = self.client.badges.name()
    self.assertEqual(len(badges), 2)

  def test_badges_recipients(self):
    res = self.client.badges.recipients()
    self.assertEqual(len(res), 2)

  def test_badges_tags(self):
    tags = self.client.badges.tags()
    self.assertEqual(len(tags), 2)

