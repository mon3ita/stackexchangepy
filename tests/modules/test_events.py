import unittest

from stackexchangepy.client import StackExchangeClient
from tests.credentials import credentials

class TestEvents(unittest.TestCase):

  def setUp(self):
    token, key = credentials()
    self.client = StackExchangeClient(access_token=token, key=key)

  def test_events_all(self):
    events, _ = self.client.events.get()
    self.assertTrue(len(events) > 0)