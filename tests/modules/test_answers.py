import unittest
import datetime as dt

from stackexchangepy.client import StackExchangeClient
from tests.credentials import credentials


class TestAnswers(unittest.TestCase):

  def setUp(self):
    token, key = credentials()
    self.client = StackExchangeClient(access_token=token, key=key)

  def test_answers_all(self):
    answers = self.client.answers.all()
    self.assertEqual(len(answers), 2)

  def test_answers_all_with_date(self):
    answers, _ = self.client.answers.all(fromdate=dt.datetime(2011, 11, 1))
    self.assertTrue(len(answers) > 0)

  def test_answers_ids(self):
    answers = self.client.answers.ids('57786933;57786932')
    self.assertEqual(len(answers), 2)

  def test_answer_comments(self):
    comments =  self.client.answers.comments('121212')
    self.assertEqual(len(comments), 2)