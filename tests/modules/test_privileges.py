import unittest

from stackexchangepy.client import StackExchangeClient
from tests.credentials import credentials

class TestPrivileges(unittest.TestCase):

  def setUp(self):
    token, key = credentials()
    self.client = StackExchangeClient(access_token=token, key=key)

  def test_privileges_all(self):
    result, _ = self.client.privileges.get()
    self.assertTrue(len(result) > 0)