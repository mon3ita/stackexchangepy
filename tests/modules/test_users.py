import unittest

from stackexchangepy.client import StackExchangeClient
from tests.credentials import credentials

class TestUsers(unittest.TestCase):

  def setUp(self):
    token, key = credentials()
    self.client = StackExchangeClient(access_token=token, key=key)

  def test_users_all(self):
    result, _ = self.client.users.all()
    self.assertTrue(len(result) > 0)