import unittest

from stackexchangepy.client import StackExchangeClient
from tests.credentials import credentials

class TestInfo(unittest.TestCase):

  def setUp(self):
    token, key = credentials()
    self.client = StackExchangeClient(access_token=token, key=key)

  def test_info(self):
    info, _ = self.client.info.get()
    self.assertTrue(len(info) > 0)