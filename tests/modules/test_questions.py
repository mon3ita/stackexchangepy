import unittest

from stackexchangepy.client import StackExchangeClient
from tests.credentials import credentials

class TestQuestions(unittest.TestCase):

  def setUp(self):
    token, key = credentials()
    self.client = StackExchangeClient(access_token=token, key=key)
  
  def test_questions_all(self):
    questions, _ = self.client.questions.all()
    self.assertTrue(len(questions) > 0)